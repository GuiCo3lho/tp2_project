#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Headers/TFather.hpp"
#include "Sources/Controller.cpp"
#include "Sources/TFather.cpp"
#include "Sources/DataStorageManager.cpp"
#include "Sources/StopWordsManager.cpp"
#include "Sources/WordFrequencyManager.cpp"

int main()
{
    vector<string> controllerInit{"init", "gameofthrones.txt", "stopwords.txt"};
    Controller a;
    a.dispatch(controllerInit);
    a.dispatch({"run"});

}







