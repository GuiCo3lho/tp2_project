
#include <iostream>
#include <vector>
#include "../Headers/Controller.hpp"
#include <bits/stdc++.h>
#include <map>
#include <set>
#include <algorithm>
#include <functional>
using namespace std;

void Controller::dispatch(vector<string> message)
{
    if (message[0] == "init")
    {
        this->ControllerTest(message[1], message[2]);
    }
    else if (message[0] == "run")
    {
        this->run();
    }
    else
    {
        cout << "Não entendi a mensagem: " << message[0] << endl;
    }
}

void Controller::ControllerTest(string data, string stopword)
{

    this->dataManager = new DataStorageManager;        //objeto datastorage
    this->stopWordsManager = new StopWordsManager;     //objeto stopwords
    this->frequencyManager = new WordFrequencyManager; //objeto wordfrequency
    this->dataManager->dispatch({"init", data});
    this->stopWordsManager->dispatch({"init", stopword});
}
pair<string, int> Controller::run()
{

    auto words = this->dataManager->dispatch({"words"});

    pair<string, int> element = {};

    for (auto word : words)
    {
        auto stopWord = this->stopWordsManager->dispatch({"isStopWord?", word});
        if (!stopWord)
        {
            this->frequencyManager->dispatch({"incrementCount", word});
        }
    }
    cout << "Sorted!" << endl;
    int contador2 = 1;

    map<string, int>::iterator it;

    auto freqListSorted = this->frequencyManager->dispatch({"sorted"});
    int contador = 1;
    cout << "Lista de words e suas frequências!" << endl;
    for (pair<string, int> element : freqListSorted)
    {
        if (contador > 26)
        {
            return element;
        }
        cout << contador << "  " << element.first << " - " << element.second << endl;
        contador++;
    }
    return element;
}
