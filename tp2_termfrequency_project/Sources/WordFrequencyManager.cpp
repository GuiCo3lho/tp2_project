#include <iostream>
#include "../Headers/WordFrequencyManager.hpp"
#include <vector>
#include <bits/stdc++.h>
#include <map>
#include <set>
#include <algorithm>
#include <functional>

using namespace std;

set<pair<__cxx11::string, int>, Comparator> WordFrequencyManager::dispatch(vector<string> message)
{
    if (message[0] == "incrementCount")
    {
        this->incrementCount(message[1]);
        return {};
    }
    else if (message[0] == "sorted")
    {
        return this->sorted();
    }
    else
    {
        cout << "Não compreendi a mensagem: " << message[0] << endl;
        return this->sorted();
    }
}

void WordFrequencyManager::incrementCount(string word)
{

    map<string, int>::iterator it;
    it = this->wordmap.find(word);
    if (it != this->wordmap.end()) //Encontrou key
    {
        it->second++;
    }
    else
    {
        this->wordmap.insert(pair<string, int>(word, 1));
    }
}

set<pair<__cxx11::string, int>, Comparator> WordFrequencyManager::sorted()
{

    Comparator compFunctor =
        [](pair<string, int> elem1, pair<string, int> elem2) {
            return elem1.second > elem2.second;
        };
    set<pair<string, int>, Comparator> setOfWords(this->wordmap.begin(), this->wordmap.end(), compFunctor);
    return setOfWords;
}

// it = find(this->wordmap.begin(), this->wordmap.end(), word);

// if (it != this->wordmap.end())
// {
//     this->wordmap.insert(pair<string, int>(word, this->freq++));
// }
// else
// {
//     this->wordmap.insert(pair<string, int>)(word, this - freq = 1)
// }
