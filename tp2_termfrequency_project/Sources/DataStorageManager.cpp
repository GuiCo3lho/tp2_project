#include <iostream>
#include "../Headers/DataStorageManager.hpp"
#include <vector>
#include <numeric>
#include <fstream>
#include <string>
#include <bits/stdc++.h>
using namespace std;

vector<string> DataStorageManager::dispatch(vector<string> message)
{

    if (message[0] == "init")
    {
        this->DataStorageManagerTest(message[1]);
        return {};
    }
    else if (message[0] == "words")
    {
        return this->getStorageManager();
    }
    else
    {
        cout << "Não entendi a mensagem: " << message[0] << endl;
        return {};
    }
}

void DataStorageManager::DataStorageManagerTest(string fileName)
{
    fstream file(fileName.c_str());
    string word;
    while (file >> word)
    {
        word.erase(remove_if(word.begin(), word.end(), [](char c) { return !isalpha(c); }), word.end());
        transform(word.begin(), word.end(), word.begin(), ::tolower);

        this->words.push_back(word);
    }
}

vector<string> DataStorageManager::getStorageManager()
{
    return this->words;
}