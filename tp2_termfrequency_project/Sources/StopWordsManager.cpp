#include <iostream>
#include "../Headers/StopWordsManager.hpp"
#include <vector>
#include <numeric>
#include <string>

using namespace std;

bool StopWordsManager::dispatch(vector<string> message)
{
    if (message[0] == "init")
    {
        this->StopWordsManagerTest(message[1]);
        return false;
    }
    else if (message[0] == "isStopWord?")
    {
        return this->isStopWords(message[1]);
    }
    else
    {
        // return false;
        cout << "Não entendi a mensagem: " << message[0] << endl;
    }
}

void StopWordsManager::StopWordsManagerTest(string fileName)
{

    vector<string> alphabet(26);
    iota(alphabet.begin(), alphabet.end(), 'a');

    ifstream file(fileName.c_str());
    if (!file)
    {
        cerr << "Cannot open the File : " << fileName << endl;
        invalid_argument("erro!");
    }

    string str;

    this->words = alphabet;
    this->words.push_back("*");
    while (getline(file, str))
    {
        this->words.push_back(str);
    }
}

vector<string> StopWordsManager::getWords()
{
    return this->words;
}

bool StopWordsManager::isStopWords(string word)
{

    //Lista de words

    for (auto var : this->words)
    {
        if (var == word)
        {

            return true;
        }
    }

    return false;
}

// for (const auto &i : alphabet)
// {
//     cout << i;
//     cout << "\n";
// }
