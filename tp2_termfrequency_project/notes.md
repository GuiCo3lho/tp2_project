

**Tasks**

1. WordsIndex

2. WordsInContext


3. TermFrequency

   1. Display the N most frequent words and corresponding frequencies ordered by decreasing value of frequency
   2. Normalize capital letters and ignore stop words "the" "for"
   3. Dont care words with equal frequencies
   4. Input
    White tigers live mostly in India
    Wild lions have nice tits and they live in India
   5. Output
    india - 2
    live - 2
    tigers - 1
    wild - 1
# PART 1
1. Process input file, counting word occurrences and writint data to a word-frequency file
   1. Hold the stop words of a file on the primary memory
   2. Read inout file one line at a time (80 chars) sizeble on primary memory
   3. Filter characters and identify words for each line and normalize capitalization
   4. Write the words and their frequencies to word_frequencies file
# PART 2
1. Iterate on the file to discover the 25 most frequently occuring words, printing them at the end.
   1. Keep an ordered list in memory holding the 25 most frequent words and their frequencies
   2. read one line at a time, each one containing the word and its frequency
   3. if the new word on file has more frequency than other words on memory, insert it sorted and delete the last word of the memory list with 25 freqs
   4. Print the 25 top most frequent words
**Styles**
1. Good Old Times
    1. Constraints
       1. Very small amount of primary memory comparing to data to be processed
       2. No identifiers "?"
        
    2. Program Notes
       1. Rotating Data through avail memory
       2. identifiers by documentation
       3. hardware and optimizing use of memory
       4. Constraints are imposed externally or self-imposed
       5. only a data memory that is accessed by indexing it with numbers
       6. concepts like freq, words, counts, sorting are absent from the program text, are represented as indexes over memory.
       7. The Only Way we can bring those concepts back in is by adding comments on the lines.
       8. Go back and forth on the code to read comments to remind ourselves what high-level concept a memory indexs corresponds to
    3. KeyWords on Code
       1. for, while, if , data class, def touch open, break, if not, del data[:] , print
       2. None Creation of new classes and none modularization.

2. Monolithic
   1. Constraints
      1. No Named Abstractions
      2. No use of libraries
   2. Style Notes
      1. Important to recognize molotiths
      2. Common to see this style
      3. Focused on make the solution work
      4. Modularization not important
      5. Big and single block of code that do all the stuff
      6. Important to try to understand the reasons of using that style
   3. KeyWords on Code
      1. for, if , else , elsif, assignments.
      2. None Vectors of abstractions
3. Things
   1. Constrains
      1. Problem split into *things* that make sense for the problem domain
      2. *thing* is a capsule that exposes or hide procedures
      3. Data is never accessed directly, only by these procedures
      4. Capsules can change procedures of other capsules
   2. Pseudo-Code
    
   ```c++
   class DataStorageManager{ //14-28 => read words from book
   public:
   
   vector <std::string> words();
   DataStorageManager(){
       <!-- reads the file -->
       <!-- normalize capitalization and non-alphanumeric characters -->
       <!-- hides the text data, providint a method to retrieve the words in it -->
       <!-- DataStorageManager is an abstraction of data and behavior related to the input text data -->
       <!--  -->
   }
   private:
   vector <std::string> words;
   
   class StopWordManager //30-43 => test if word is a stopwords or not
   <!-- App doesnt need to know what kind of data that class has -->
   <!-- == == how is decided stopword/keyword process -->
   <!-- Determine stopword/keyword -->

   private:
   vector <std::string> stopwords;
   public:
   bool is_stop_word();
   }

   class WordFrequencyManager{ //45-61 => Invoked when not a stopword, increment the count for that word
       public:
       increment_count();// 
       sorted(); //return sorted collection of words by freq
       

   }

   class WordFrequencyController{ //63-76       
   <!-- Class that starts it all -->
   <!-- Constructor instantiate all other app objcets -->
   public:
   run(); // retrive a sorted collection of words from WFM and displays the 25 words most frequent in text file
       
   }

   main() {//instantieates a WFC
   WordFrequencyController test;
   test.run();
   }

4. Cons
   1. difficult when have a distrbuited systems that shares code with many people that have different languages and different infrastructure of software, different software engineering
   2. 









# Natural Brasil Açai
1. Açai com xarope de Guaraná e banana
(10 Litros)
- Popular - 59,99 => 6 R$/L 
- Médio - 74,90 => 7,90 R$/L
- Especial - 99,90 => 10 R$/L 

2. Complementos
3. Descartáveis
   1. Copo Liso
      300 ml   -  4,20 R$  (50 copos)
      400 ml   -  6,70 R$  (50 copos)
      500 ml   -  8,25 R$  (50 copos
         )
   2. Colher Descartável
      3,10 R$ (50 colheres)
- 

- [x] Comprar Açai xarope de guaraná com  banana (10L)
- [x] Comprar Descartáveis
- [x] Comprar Leite Condensado e Leite Ninho
- [ ] Juntar gente para organizar o stand
- [ ] Conseguir gente para ficar no stand durante o período de vendas
- [ ] Combinar preço de venda levando em consideração os complementos (Leite Ninho, Leite condensado,etc)






    
