#include <gtest/gtest.h>
#include <iostream>
#include "../Sources/Controller.cpp"
#include "../Sources/TFather.cpp"
#include "../Sources/DataStorageManager.cpp"
#include "../Sources/StopWordsManager.cpp"
#include "../Sources/WordFrequencyManager.cpp"
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <set>
using namespace std;

TEST(WordControllerTEST, setOfWordsReturnTest)
{
    ASSERT_TRUE(false);
}

TEST(Teste, setoftext)
{
    Controller a;
    a.dispatch({"init", "gameofthrones.txt", "stopwords.txt"});
    a.dispatch({"run"});
}
TEST(WordFrequencyManagerTEST, dispatchTEST)
{
    vector<string> messageInc = {"incrementCount", "passaro"};
    vector<string> messageInc2 = {"incrementCount", "ícaro"};
    vector<string> messageInc3 = {"incrementCount", "fernando"};
    vector<string> messageWords = {"sorted"};
    vector<string> messageWrong = {"rinosceronte"};
    map<string, int>::iterator it;
    int contador = 0;
    int contador2 = 0;
    WordFrequencyManager a;
    a.dispatch(messageWrong);
    a.dispatch(messageInc2);
    a.dispatch(messageInc2);
    a.dispatch(messageInc2);
    a.dispatch(messageInc2);
    a.dispatch(messageInc3);
    a.dispatch(messageInc3);
    a.dispatch(messageInc3);
    a.dispatch(messageInc);
    a.dispatch(messageInc);
    auto b = a.dispatch(messageWords);
    for (pair<string, int> element : b)
    {
        if (contador > 24)
        {
            return;
        }
        cout << contador << "  " << element.first << " - " << element.second << endl;
        contador++;
    }
}
TEST(StopWordsManagerTEST, dispatchTEST)
{
    vector<string> messageInit = {"init", "stopwords.txt"};
    vector<string> messageWords = {"isStopWord?", "the"};
    vector<string> messageWrong = {"sabao"};
    StopWordsManager a;
    a.dispatch(messageWrong);
    a.dispatch(messageInit);

    auto b = a.dispatch(messageWords);
    ASSERT_TRUE(b);
}
TEST(StopWordsManagerTEST, isStopWord)
{
    StopWordsManager container;
    container.dispatch({"init", "stopwords.txt"});
    auto containerP = container.dispatch({"isStopWord?", "the"});
    ASSERT_TRUE(containerP);
}
TEST(StopWordsManagerTEST, stopWordsVerification)
{
    // StopWordsManager container;
    // container.dispatch({"init", "stopwords.txt"});
    // auto sw = container.dispatch({"words"});
    // TO DO
}

TEST(DataStorageManagerTEST, dispatchTEST)
{
    DataStorageManager storageBook;
    storageBook.dispatch({"init", "gameofthrones.txt"});
    auto storageWords = storageBook.dispatch({"words"});
    EXPECT_EQ("josue", storageWords[1]);        
}   
