#ifndef _StopWordsManager_H_
#define _StopWordsManager_H_
#include <iostream>
#include "TFather.hpp"
#include <vector>
using namespace std;
class StopWordsManager : public TFather
{
private:
    vector<string> words = {};
    bool isStopWords(string);  //retorna true se é stop word
    vector<string> getWords(); //retorna vetor de stopwords
    void printInfo()
    {
        cout << "Sou da classe StopWordsManager!\n";
    }
    void StopWordsManagerTest(string); //gera vetor de stopwords

public:
    bool dispatch(vector<string> messages);
};

#endif