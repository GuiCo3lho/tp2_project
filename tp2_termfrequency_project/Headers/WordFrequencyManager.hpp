#ifndef _WordFrequencyManager_H_
#define _WordFrequencyManager_H_
#include <iostream>
#include <vector>
#include <bits/stdc++.h>
#include <map>
#include <set>
#include <algorithm>
#include <functional>
using namespace std;
typedef function<bool(pair<string, int>, pair<string, int>)> Comparator;

class WordFrequencyManager : public TFather
{
private:
    map<string, int> wordmap = {};

    void incrementCount(string);

    set<pair<string, int>, Comparator> sorted();
    map<string, int> getWordmap()  {
        return this->wordmap;
    }

public:
    set<pair<__cxx11::string, int>, Comparator> dispatch(vector<string> message);
};

#endif