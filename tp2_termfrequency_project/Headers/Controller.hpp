#ifndef _Controller_H_
#define _Controller_H_
#include "TFather.hpp"
#include "DataStorageManager.hpp"
#include "StopWordsManager.hpp"
#include "WordFrequencyManager.hpp"
#include <set>
#include <map>

class Controller : public TFather
{
   

private:
    DataStorageManager *dataManager;
    StopWordsManager *stopWordsManager;
    WordFrequencyManager *frequencyManager;

    void ControllerTest(string, string);
    pair<string, int> run();
public:
    void dispatch(vector<string> message);
};

#endif