#ifndef _DataStorage_H_
#define _DataStorage_H_
#include <iostream>
#include "TFather.hpp"
#include <vector>
using namespace std;

class DataStorageManager : public TFather
{
private:
    vector<string> words = {};
    vector<string> getStorageManager(); //retorna vetor de words
    void printInfo()
    {
        cout << "Sou da classe DataStorageManager!\n";
    }
    void DataStorageManagerTest(string); //Gera vetor de words

public:
    vector<string> dispatch(vector<string>);
};
#endif