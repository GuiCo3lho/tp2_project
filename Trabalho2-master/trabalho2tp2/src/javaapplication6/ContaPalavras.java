package javaapplication6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;

public class ContaPalavras {
 
    public static void main(String[] args) throws Exception {
      
    File dracula = new File("C:/Users/pedro/Desktop/dracula.txt");
          
    Map<String,Integer> mapPalavras; 
         
    mapPalavras = new HashMap<>();
         
        FileReader txtFile = new FileReader(dracula);
        BufferedReader txtBuffer = new BufferedReader(txtFile);
        String linha; 
               
        linha = txtBuffer.readLine();
        
         
        while (linha != null) {
             
            String minusculo = linha.toLowerCase();
             
            Pattern p = Pattern.compile("(\\d+)|([a-záéíóúçãõôê]+)");
            Matcher m = p.matcher(minusculo);
 
             
            while(m.find())
            {
              String token = m.group(); 
              Integer contador = mapPalavras.get(token); 
                                       
                 
                if (contador != null) { 
                    mapPalavras.put(token, contador+1);
                }
                else { 
                    mapPalavras.put(token,1);
                }
            }
             
            linha = txtBuffer.readLine();
        }
         
        txtBuffer.close();
 
     for (Map.Entry<String, Integer> entry : mapPalavras.entrySet()) {
         if(entry.getValue() >=200){
             System.out.println(entry.getKey() + " - " + entry.getValue());
         }      
     }
         
   }
 
}
